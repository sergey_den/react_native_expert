import React from 'react';
import {
  StyleSheet,
  SafeAreaView,
  View,
  Image,
  Text,
  TouchableOpacity,
} from 'react-native';
// import {launchImageLibrary} from 'react-native-image-picker';
import axios from 'axios';

// const options = {
//   title: 'Select image',
//   type: 'library',
//   options: {
//     maxHeight: 200,
//     maxWidth: 200,
//     selectionLimit: 1,
//     mediaType: 'photo',
//     includeBase64: false,
//   },
// };

const SERVER_URL = 'http://localhost:4000';

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: 350,
  },
  mainBody: {
    flex: 1,
    justifyContent: 'center',
    padding: 20,
  },
  buttonStyle: {
    backgroundColor: '#307ecc',
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#307ecc',
    height: 40,
    alignItems: 'center',
    borderRadius: 30,
    marginLeft: 35,
    marginRight: 35,
    marginTop: 15,
  },
  buttonTextStyle: {
    color: '#FFFFFF',
    paddingVertical: 10,
    fontSize: 16,
  },
  textStyle: {
    backgroundColor: '#fff',
    fontSize: 15,
    marginTop: 16,
    marginLeft: 35,
    marginRight: 35,
    textAlign: 'center',
  },
  imageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#d9d6d6',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 5,
  },
  titleContainer: {
    alignItems: 'center',
    marginBottom: 30,
  },
  title: {
    fontSize: 23,
    fontWeight: 'bold',
  },
});

function App() {
  const [photo, setPhoto] = React.useState(null);

  const handleChoosePhoto = async () => {
    // const result = await launchImageLibrary(options);
    // setPhoto(result.assets[0]);
    fetch('https://jsonplaceholder.typicode.com/todos/1')
      .then((response) => response.json())
      .then((json) => console.log(json));
  };

  const handleUploadPhoto = async () => {
    const formData = new FormData();

    formData.append('photo', {
      name: photo.filename,
      type: photo.type,
      uri: photo.uri,
    });

    try {
      const response = await axios.post(`${SERVER_URL}/api/upload`, formData, {
        headers: { 'Content-Type': 'multipart/form-data' },
      });

      const data = await response.json();
      console.log(data);
    } catch (error) {
      console.log('Error: ', error);
    }
  };

  const resetPhoto = () => {
    setPhoto(null);
  };

  return (
    <SafeAreaView style={styles.mainBody}>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>React Native Image Upload</Text>
      </View>

      {photo ? (
        <View>
          <Image source={{ uri: photo.uri }} style={styles.image} />
          <TouchableOpacity
            style={styles.buttonStyle}
            activeOpacity={0.5}
            onPress={handleUploadPhoto}
          >
            <Text style={styles.buttonTextStyle}>Upload Image</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.buttonStyle}
            activeOpacity={0.5}
            onPress={resetPhoto}
          >
            <Text style={styles.buttonTextStyle}>Discard Image</Text>
          </TouchableOpacity>
        </View>
      ) : (
        <View>
          <TouchableOpacity
            style={styles.buttonStyle}
            activeOpacity={0.5}
            onPress={handleChoosePhoto}
          >
            <Text style={styles.buttonTextStyle}>Choose photo</Text>
          </TouchableOpacity>
        </View>
      )}
    </SafeAreaView>
  );
}

export default App;
